// Non-volatile memory interface
// Accesses EEPROM for Atmega32U4 devices and preferences for ESP32
// Limited by Atmega32U4 1024 bytes of EEPROM memory

// 0 - 14      inputs[0]
// 0 - 224     inputs[*]
// 512 - 543   MATRIX_PIN_ASSIGNMENTS (32)    
// 544 - 799   MATRIX_BUTTON_ASSIGNMENTS (256)
// 800 - 847   ENCODER_ASSIGNEMENTS (48)
// 1000 - 1015 DEVICE_NAME
// 1022        I2C_ADDRESS
// 1024        FIRSTRUN (100 == false)

// ATMEGA 2560
// 0 - 14      inputs[0]
// 0 - 882     inputs[*]
// 2012 - 2043   MATRIX_PIN_ASSIGNMENTS (32)    
// 2044 - 2299   MATRIX_BUTTON_ASSIGNMENTS (256)
// 2300 - 2347   ENCODER_ASSIGNEMENTS (48)
// 4000 - 4015 DEVICE_NAME
// 4022        I2C_ADDRESS
// 4024        FIRSTRUN (100 == false)

#if defined(ESP32)
#include <Preferences.h>
Preferences prefs;

void Store8BitValue(uint16_t idx, uint8_t val)
{
    prefs.begin("realrobots");

    char key[16];
    itoa(idx, key, 10);

    prefs.putUChar(key, val);
    prefs.end();
}

void Store16BitValue(uint16_t idx, int val)
{
    prefs.begin("realrobots");

    char key[16];
    itoa(idx, key, 10);

    prefs.putShort(key, val);
    prefs.end();
}

uint8_t Read8BitValue(uint16_t idx)
{
    prefs.begin("realrobots");

    char key[16];
    itoa(idx, key, 10);

    uint8_t returnVal = prefs.getUChar(key);
    prefs.end();
    return returnVal;
}

uint16_t Read16BitValue(uint16_t idx)
{
    prefs.begin("realrobots");

    char key[16];
    itoa(idx, key, 10);

    uint16_t returnVal = prefs.getShort(key);
    prefs.end();
    return returnVal;
}

#else
#include <EEPROM.h>

void Store8BitValue(uint16_t idx, uint8_t val)
{
    EEPROM.write(idx, val);
}

void Store16BitValue(uint16_t idx, int val)
{
    EEPROM.write(idx, val);
    EEPROM.write(idx + 1, val >> 8);
}

uint8_t Read8BitValue(uint16_t idx)
{
    return EEPROM.read(idx);
}

int Read16BitValue(uint16_t idx)
{
    int val;

    val = (EEPROM.read(idx + 1) << 8);
    val |= EEPROM.read(idx);

    return val;
}



#endif
